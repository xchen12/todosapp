package edu.towson.cosc431.chen.todos

interface IFragmentController {

    fun handleAllTv()
    fun handleActiveTv()
    fun handleCompTv()

}