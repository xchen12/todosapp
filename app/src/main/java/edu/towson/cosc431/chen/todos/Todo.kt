package edu.towson.cosc431.chen.todos

import java.util.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.text.DateFormat

data class Todo (
        val id: Int,
        val title :String,
        val content : String,
        val isComplete : Boolean,
        val avatarUrl: String
        )

data class ApiResult(val data: List<Todo>)
{

       fun toJson(): String{
            return Gson().toJson(this)
   }
}

