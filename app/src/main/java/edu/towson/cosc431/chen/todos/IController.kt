package edu.towson.cosc431.chen.todos

import android.graphics.Bitmap

interface IController {
   // fun toggleComplete(todo:Todo)
   // fun addTodo(todo: Todo)
  //  fun removeTodo(todo:Todo)
    val TodoList: List<Todo>
    fun fetchAvatar(url: String, callback: (Bitmap?)->Unit)
}