package edu.towson.cosc431.chen.todos


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_todo.view.*

class todoFrag : Fragment(),IFragmentUpdate {

    override lateinit var controller: IFragmentController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_todo, container, false)

        view.all_btn.setOnClickListener {
                controller.handleAllTv()
        }

        view.active_btn.setOnClickListener {
                controller.handleActiveTv()
        }

        view.comp_btn.setOnClickListener {
                controller.handleCompTv()
        }
        return view


    }



}
