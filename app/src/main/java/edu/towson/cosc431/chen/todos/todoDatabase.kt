/*
package edu.towson.cosc431.chen.todos

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

object TodoContract {
    object TodoEntry {
        const val TABLE_NAME = "todos"
        const val COLUMN_NAME_ID = "todos_id"
        const val COLUMN_NAME_TITLE = "todos_title"
        const val COLUMN_NAME_CONTENT = "todos_content"
        const val COLUMN_NAME_ISCOMPLETED = "todos_complete"
        const val COLUMN_NAME_DATE = "todos_date"
        const val COLUMN_NAME_DELETED = "todos_deleted"
    }
}

interface IDatabase {
    fun addTodos(todo: Todo)
    fun getTodos(): List<Todo>
    fun getTodos(id: String): Todo?
    fun deleteTodo(todo: Todo)
    fun updateTodo(todo: Todo)
}

private const val SQL_CREATE_ENTRIES =
        "CREATE TABLE ${TodoContract.TodoEntry.TABLE_NAME} ( " +
                "${TodoContract.TodoEntry.COLUMN_NAME_ID} TEXT PRIMARY KEY, " +
                "${TodoContract.TodoEntry.COLUMN_NAME_TITLE} TEXT, " +
                "${TodoContract.TodoEntry.COLUMN_NAME_CONTENT} TEXT, " +
                "${TodoContract.TodoEntry.COLUMN_NAME_DATE} TEXT, " +
                "${TodoContract.TodoEntry.COLUMN_NAME_DELETED} INTEGER DEFAULT 0, " +
                "${TodoContract.TodoEntry.COLUMN_NAME_ISCOMPLETED} INTEGER DEFAULT 0)"


private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${TodoContract.TodoEntry.TABLE_NAME}"

class TodosDatabase(ctx: Context) : IDatabase {

    override fun getTodos(id: String): Todo{

        val cursor = db
                .rawQuery(
                        "SELECT * FROM ${TodoContract.TodoEntry.TABLE_NAME} "
                                + "WHERE ${TodoContract.TodoEntry.COLUMN_NAME_ID}= ?"
                        , null
                )
        lateinit var result: Todo
        while(cursor.moveToNext()) {
            val id: String = cursor
                    .getString(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_ID)
                    )

            val title:String = cursor
                    .getString(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TITLE)
                    )

            val content:String = cursor
                    .getString(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_CONTENT)
                    )


            val isCompletedAsInt = cursor
                    .getInt(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_ISCOMPLETED)
                    )


            val todos = Todo(id = null
                    , title = title
                    , isComplete = isCompletedAsInt == 1
                    , content = content
                    , avatarUrl = toString()
            )
            result = todos
        }

        cursor.close()
        return result
    }

    override fun updateTodo(todos: Todo) {

        val contentValues = toContentValues(todos)

        db.update(
                TodoContract.TodoEntry.TABLE_NAME,
                contentValues,
                "${TodoContract.TodoEntry.COLUMN_NAME_ID} = ?",
                arrayOf(todos.id.toString())
        )
    }

    override fun deleteTodo(todos: Todo) {

        val contentValues = toContentValues(todos)
        db.delete(
                TodoContract.TodoEntry.TABLE_NAME,
                "${TodoContract.TodoEntry.COLUMN_NAME_ID} = ?",
                arrayOf(todos.id.toString())
        )

    }

    override fun addTodos(todos: Todo) {
        val contentValues = toContentValues(todos)

        db.insert(
                TodoContract.TodoEntry.TABLE_NAME, // table name
                null, // nullColumnHack -- look it up!
                contentValues
        )

    }

    override fun getTodos(): List<Todo> {

        val cursor = db
                .rawQuery(
                        "SELECT * FROM ${TodoContract.TodoEntry.TABLE_NAME} "
                                + "WHERE ${TodoContract.TodoEntry.COLUMN_NAME_DELETED}=0"
                        , null
                )
        val result = mutableListOf<Todo>()
        while(cursor.moveToNext()) {
            val id: String = cursor
                    .getString(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_ID)
                    )

            val title:String = cursor
                    .getString(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TITLE)
                    )

            val content:String = cursor
                    .getString(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_CONTENT)
                    )


            val isCompletedAsInt = cursor
                    .getInt(
                            cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_ISCOMPLETED)
                    )


            val todos = Todo(id = toString()
                    , title = title
                    , isComplete = isCompletedAsInt == 1
                    , content = content
                    , avatarUrl = toString()
            )
            result.add(todos)
        }

        cursor.close()
        return result
    }

    private fun toContentValues(todo: Todo): ContentValues {
        val cv = ContentValues()

        cv.put(TodoContract.TodoEntry.COLUMN_NAME_ID, todo.id.toString())
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_TITLE, todo.title)
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_CONTENT, todo.content)

        val isCompletedAsInt = when(todo.isComplete) {
            true -> 1
            false -> 0
        }
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_ISCOMPLETED, isCompletedAsInt)


        return cv
    }

    private val db: SQLiteDatabase

    init {
        db = SongDbHelper(ctx).writableDatabase
    }

    class SongDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION)
    {
        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(SQL_CREATE_ENTRIES)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db?.execSQL(SQL_DELETE_ENTRIES)
            onCreate(db)
        }

        companion object {
            val DATABASE_NAME = "todos.db"
            val DATABASE_VERSION = 9
        }
    }
}

*/
