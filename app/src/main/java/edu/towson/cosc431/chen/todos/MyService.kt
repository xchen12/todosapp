/*
package edu.towson.cosc431.chen.todos

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor

class MyService : Service() {

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    val executor: ThreadPoolExecutor = Executors.newFixedThreadPool(4) as ThreadPoolExecutor

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val id = THREAD_ID++

        executor.execute {
            val copy = id
            showNotification(copy, 0)
            try {
                (0..10).forEach {
                    Thread.sleep(500)
                    showNotification(copy, it)
                }
            } catch(e: Exception) {
            } finally {
                LocalBroadcastManager
                        .getInstance(this)
                        .sendBroadcast(Intent("MY_ACTION").putExtra(NOTIF_ID, copy))
            }
        }

        return START_NOT_STICKY
    }

    private fun showNotification(id: Int, progress: Int) {
        val intent = Intent(this, MainActivity::class.java)
                .putExtra("EXTRA", "This is a meesage from a pending intent")
                .putExtra(NOTIF_ID, id)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val builder = NotificationCompat.Builder(this, "0")
        builder.setContentTitle("Hello from ExampleService (${id})")
                .setContentText("Click this notification to launch the Activity")
                .setSmallIcon(android.R.drawable.btn_dialog)
                .setVibrate(longArrayOf(1000L))
                .setContentIntent(pendingIntent) // the intent to launch the activity
                .setProgress(10, progress, false)
                .setTicker("Hello World")
                .setAutoCancel(true) // make sure the notification closes on click
        val notification = builder.build()
        NotificationManagerCompat.from(this).notify(id, notification)
    }
    companion object {
        var THREAD_ID = 0
        val NOTIF_ID = "notification_id"
    }
}
*/
