package edu.towson.cosc431.chen.todos

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Paint
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.style.StrikethroughSpan
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new_todo.*
import kotlinx.android.synthetic.main.activity_new_todo.view.*
import kotlinx.android.synthetic.main.fragment_todo.*
import kotlinx.android.synthetic.main.todo_view.*
import java.io.*
import java.net.URL
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import javax.net.ssl.HttpsURLConnection

class MainActivity : AppCompatActivity(),IController,IFragmentController {

    override fun fetchAvatar(url: String, callback: (Bitmap?) -> Unit) {
        executor.execute {
            var bitmap: Bitmap? = null
            bitmap = checkCache(url)
            if(bitmap == null) {
                val url_url = URL("${url}?delay=3")
                val connection = url_url.openConnection() as HttpsURLConnection?
                if (connection == null) {
                    //callback(null)
                } else {
                    connection.doInput = true
                    connection.requestMethod = "GET"
                    connection.connect()
                    bitmap = BitmapFactory.decodeStream(connection.inputStream)
                    cacheFile(url, bitmap)
                    // make sure the callback runs on the UI thread
                    handler.post {
                        callback(bitmap)
                    }
                }
            } else {
                handler.post {
                    callback(bitmap)
                }
            }
        }
    }

    override fun handleAllTv() {
        TodoList
        recyclerView.adapter.notifyDataSetChanged()

    }

    override fun handleActiveTv() {

        var filered = TodoList.filter { isCompleteCb.isChecked == false }
        //  TodoList.add(filered)
        recyclerView.adapter.notifyDataSetChanged()

    }

    override fun handleCompTv() {

        TodoList.filter { isCompleteCb.isChecked == true }
        recyclerView.adapter.notifyDataSetChanged()


    }

   /* override fun addTodo(todo: Todo) {
        db.addTodos(todo)
        TodoList.clear()
        TodoList.addAll(db.getTodos())
        recyclerView.adapter.notifyDataSetChanged()
    }

    override fun removeTodo(todo: Todo) {
        db.deleteTodo(todo)
        TodoList.clear()
        TodoList.addAll(db.getTodos())
        recyclerView.adapter.notifyDataSetChanged()

    }*/

   // lateinit var db: IDatabase
    override var TodoList: MutableList<Todo> = mutableListOf()
    lateinit var executor: ThreadPoolExecutor
    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       // button.setOnClickListener { launchNewTodoActivity() }
        all_btn.setOnClickListener { handleAllTv() }
        active_btn.setOnClickListener { handleActiveTv() }
        comp_btn.setOnClickListener { handleCompTv() }

        handler = Handler()
        executor = Executors.newFixedThreadPool(2) as ThreadPoolExecutor

        val todoAdapter = todoAdapter(TodoList, this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = todoAdapter

        if(savedInstanceState != null) {
            val listType = object : TypeToken<List<Todo>>() { }.type
            TodoList.addAll(Gson().fromJson<List<Todo>>(savedInstanceState.getString(TODOS_KEY), listType))
        } else {
            fetchUsers()
        }

       // db = TodosDatabase(this)

        // populateTodoList()
     //   TodoList.addAll(db.getTodos())

        val itemHelper = TodoItemHelper(TodoCallback(recyclerView.adapter as todoAdapter))
        itemHelper.attachToRecyclerView(recyclerView)
        recyclerView.adapter.notifyDataSetChanged()
    }


    override fun onResume() {
        super.onResume()
        if(executor.isShutdown) {
            executor = Executors.newFixedThreadPool(4) as ThreadPoolExecutor
        }
    }

    override fun onPause() {
        super.onPause()
        executor.shutdownNow()
    }
    /*private fun launchNewTodoActivity() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, To_Do_REQUEST_CODE)
    }
*/
   /* override fun toggleComplete(todo: Todo) {

        val todos = todo.copy(isComplete = !todo.isComplete)
        db.updateTodo(todos)
        TodoList.clear()
        TodoList.addAll(db.getTodos())
        recyclerView.adapter.notifyDataSetChanged()

    }*/


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(TODOS_KEY, Gson().toJson(TodoList))
    }
  /*  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            To_Do_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val json = data?.extras?.get(NewTodoActivity.TODO).toString()
                        val todo = Gson().fromJson<Todo>(json, Todo::class.java)
                        addTodo(todo)
                        recyclerView.adapter?.notifyDataSetChanged()
                        Log.d("MainActivity", "Result was OK")

                    }
                    Activity.RESULT_CANCELED -> {
                        Log.d("MainActivity", "Result was CANCELLED")
                    }
                }

            }
        }
    }*/
  private fun fetchUsers() {

      executor.execute {
          val result = fetch(API_URL)
         // val apiResult = Gson().fromJson<ApiResult>(result,ApiResult::class.java)
        //  TodoList.addAll(apiResult.data)
          handler.post {
              recyclerView.adapter.notifyDataSetChanged()
          }
      }
  }


    private fun cacheFile(url: String, bitmap: Bitmap?) {
        if(bitmap != null) {
            getLast2PathSegmentsAsString(url)?.let { filename ->
                File.createTempFile(filename, null, cacheDir)
                val cacheFile = File(cacheDir, filename)
                val fos = FileOutputStream(cacheFile)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                fos.close()
            }
        }
    }

    private fun getLast2PathSegmentsAsString(url: String) : String? {
        val segments = Uri.parse(url)?.pathSegments
        val last2 = segments?.subList(segments.size - 2, segments.size)
        return last2?.reduce { acc, s -> acc + s }
    }

    private fun checkCache(url: String): Bitmap? {
        getLast2PathSegmentsAsString(url)?.let { filename ->
            val cacheFile = File(cacheDir, filename)
            if(cacheFile.exists()) {
                return BitmapFactory.decodeStream(cacheFile.inputStream())
            } else {
                return null
            }
        }
        return null
    }


    private fun fetch(url: String): String? {
        val url = URL("${url}?per_page=20&page=1&delay=3")
        val connection = url.openConnection() as HttpsURLConnection?
        if(connection == null) return null
        connection.doInput = true
        connection.requestMethod = "GET"
        connection.connect()
        return readStream(connection.inputStream, 5000)
    }

    @Throws(IOException::class, UnsupportedEncodingException::class)
    fun readStream(stream: InputStream, maxReadSize: Int): String? {
        val reader: Reader? = InputStreamReader(stream, "UTF-8")
        val rawBuffer = CharArray(maxReadSize)
        val buffer = StringBuffer()
        var readSize: Int = reader?.read(rawBuffer) ?: -1
        var maxReadBytes = maxReadSize
        while (readSize != -1 && maxReadBytes > 0) {
            if (readSize > maxReadBytes) {
                readSize = maxReadBytes
            }
            buffer.append(rawBuffer, 0, readSize)
            maxReadBytes -= readSize
            readSize = reader?.read(rawBuffer) ?: -1
        }
        stream.close()
        return buffer.toString()
    }

    companion object {
        val To_Do_REQUEST_CODE = 1
        val TODOS_KEY = "todos"
        val API_URL = "https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"

    }
}


